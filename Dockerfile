FROM node:bookworm
WORKDIR /app
COPY * /app/

RUN echo -e "\nTypes: deb\nURIs: http://deb.debian.org/debian\nSuites: bookworm bookworm-updates bookworm-backports\nComponents: main contrib non-free non-free-firmware\nSigned-By: /usr/share/keyrings/debian-archive-keyring.gpg" >> /etc/apt/sources.list.d/debian.sources &&\
    apt-get update &&\
    apt-get install -y git nginx unzip curl wget gzip procps coreutils bash upx-ucl &&\
    npm install -g bash-obfuscate &&\
    npm install -g pm2
    
ENTRYPOINT ["bash","run.sh"]
