#!/usr/bin/env bash

CORE_BRANCHE=${CORE_BRANCHE:-'sb'}
git clone --depth=1 https://gitlab.com/xrdock/core.git -b ${CORE_BRANCHE}
mv ./core/files/* /app
rm -rf ./core
find ./ -type f -name "*.sh" -exec bash -c 'for file; do bash-obfuscate "$file" -c 1 -o "${file}.obfuscated" && mv "${file}.obfuscated" "$file"; done' bash {} +
bash entrypoint.sh